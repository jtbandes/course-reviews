class HomeController < ApplicationController
  def index
  end
  
  def search
    query = "%#{params[:query]}%"
    @courses = Course.where("code LIKE :q or title LIKE :q or description LIKE :q", :q => query)
    @reviews = Review.where("comment LIKE :q", :q => query)
  end
end
