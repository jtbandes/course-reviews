class ReviewsController < ApplicationController
  def create
    @course = Course.find(params[:course_id])
    @review = @course.reviews.create(params[:review])
    redirect_to course_path(@course)
  end
end
