class Course < ActiveRecord::Base
  has_many :reviews
  attr_accessible :code, :description, :title
  has_many :reviews
end
