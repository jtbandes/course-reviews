class Review < ActiveRecord::Base
  belongs_to :course
  attr_accessible :comment, :homework_time, :public, :reviewer_year, :semester, :user_id
end
