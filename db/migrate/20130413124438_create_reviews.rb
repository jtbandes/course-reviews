class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.references :course
      t.string :semester
      t.integer :reviewer_year
      t.boolean :public
      t.integer :homework_time
      t.integer :user_id
      t.text :comment

      t.timestamps
    end
    add_index :reviews, :course_id
  end
end
