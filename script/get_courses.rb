require 'open-uri'
require 'json'
require '/home/ncarter/Documents/hack/courserev/course-reviews/config/environment.rb'

departments = JSON.parse(open("http://course-api.herokuapp.com/").read)


departments.keys.each do |dept|

	begin
		courses = JSON.parse(open("http://course-api.herokuapp.com"+dept).read)
	rescue
		next
	end

	courses.each do |thiscourse|

		if thiscourse['title'] != "" && Course.where(:code => thiscourse['course']).empty?

			@course = Course.new(:code => thiscourse['course'])
			@course.title = thiscourse['title']
			@course.description = thiscourse['instructor']

			@course.save
		end

	end
end



